#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include "cli_microrl.h"
#include "hmi_msg.h"
#include "print_helper.h"
#include "rfid.h"
#include "andygock_avr-uart/uart.h"
#include "helius_microrl/microrl.h"
#include "hd44780_111/hd44780.h"
#include "matejx_avr_lib/mfrc522.h"
#include "andy_brown_memdebug/memdebug.h"

#define ASCII_CHARS 128
#define LED_HEARTBEAT PORTA0 /* Arduino Mega digital pin 22 */
#define UART_BAUD 9600
#define UART_STATUS_MASK 0x00FF

/* Create a command line interface instance */
static microrl_t rl;
static microrl_t *prl = &rl;


static inline void init_leds(void)
{
    /* lab2 LEDs set up code goes here */
    /* The following pin configuration is given for Arduino Mega. */
    /* Set port A pins 0, 2, 4 for output (digital pins 22, 24, 26) */
    DDRA |= _BV(DDA0) | _BV(DDA2) | _BV(DDA4);
    DDRB |= _BV(DDB7); /* Set port B pin 7 for output (dig. pin 13) */
    PORTB &= ~_BV(PORTB7); /* Turn onboard LED off */
}


/* Init console as stdout/stdin in UART0 */
static inline void init_stdcon(void)
{
    uart0_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
}


/* Init error console as stderr in UART1 and print user code info */
static inline void init_errcon(void)
{
    uart1_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    uart1_puts_p(ver_fw);
    uart1_puts_p(ver_libc);
}


static inline void init_sys_timer(void)
{
    TCCR1A = 0;
    TCCR1B = _BV(CS12) | _BV(WGM12); // Turn on CTC (Clear Timer on Compare)
    OCR1A = 62549; // Note that it is actually two registers OCR5AH and OCR5AL
    TIMSK1 |= _BV(OCIE1A); // Output Compare A Match Interrupt Enable
}


/* Initialize timer to read RFID tags periodically. Using timer 3 */
static inline void init_rfid_timer(void)
{
    TCCR5A = 0;
    TCCR5B = _BV(WGM52) | _BV(CS52);
    OCR5A = 31250; // Note that it is actually two registers OCR5AH and OCR5AL
    TIMSK5 = _BV(OCIE5A); /// Output Compare A Match Interrupt Enable
}


/* Initialize LCD display and display student's name */
static inline void init_display(void)
{
    lcd_init();
    lcd_clrscr(); /* Clearing the screen and moving the cursor to the start point*/
    lcd_puts_P(stud_name); /* Print student's name */
}


/* Initialize RFID reader */
static inline void init_rfid_reader(void)
{
    MFRC522_init();
    PCD_Init();
}


/* This code was made regarding the snippet from Silver Kits repository.
https://gitlab.com/I237-2017/code-snippets/snippets/1686121 */

static inline void heartbeat(void)
{
    static time_t prev_time;
    char ascii_buf[11] = {0x00};
    time_t now = time(NULL);

    if (prev_time != now)
    {
        /* Print uptime to uart1 */
        ltoa(now, ascii_buf, 10);
        uart1_puts_p(PSTR("Uptime: "));
        uart1_puts(ascii_buf);
        uart1_puts_p(PSTR(" s.\r\n"));
        PORTA ^= _BV(LED_HEARTBEAT); /* Toggle LED */
        prev_time = now;
    }
}


void main(void)
{
    init_sys_timer(); /* Initialize system timer */
    init_rfid_timer(); /* Initialize RFID timer */
    init_leds(); /* Configure LED ports */
    init_errcon(); /* Initialize error console */
    init_stdcon(); /* Initialize main console */
    init_display(); /* Initialize LCD display */
    init_rfid_reader(); /* Initialize RFID reader */
    sei(); /* Enable interrupts */

    uart0_puts_p(stud_name); /* Print student's name in console */
    uart0_puts_p(crlf);
    microrl_init(prl, uart0_puts); /* Calling init with pointer to microrl instance and print callback */
    microrl_set_execute_callback(prl, cli_execute); /* Setting callback for execute */

    while (1) {
        microrl_insert_char(prl, (uart0_getc() & UART_STATUS_MASK));
        heartbeat();
        rfid_scan();
    }
}

/* Counter 1 ISR (interrupt service routine) */
ISR(TIMER1_COMPA_vect)
{
    system_tick();
}

/* Counter 5 ISR */
ISR(TIMER5_COMPA_vect)
{
    rfid_queries++;
}
