#ifndef PRINT_HELPER_H
#define PRINT_HELPER_H

void print_ascii_tbl(void);
void print_for_human(const unsigned char *array, const size_t len);
void print_char_hex(const unsigned char c);

#endif /* PRINT_HELPER_H */
