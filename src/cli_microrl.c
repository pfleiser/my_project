//TODO There are most likely unnecessary includes. Clean up during lab6
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <avr/pgmspace.h>
#include "andygock_avr-uart/uart.h"
#include "hd44780_111/hd44780.h"
#include "helius_microrl/microrl.h"
#include "andy_brown_memdebug/memdebug.h"
#include "rfid.h"
#include "hmi_msg.h"
#include "cli_microrl.h"
#include "print_helper.h"

#define ASCII_CHARS 128
#define NUM_ELEMS(x)        (sizeof(x) / sizeof((x)[0]))

const char help_cmd[] PROGMEM = "help";
const char help_help[] PROGMEM = "Get help";
const char example_cmd[] PROGMEM = "example";
const char example_help[] PROGMEM =
    "Prints out all provided 3 arguments. Usage: example <argument> <argument> <argument>";
const char ver_cmd[] PROGMEM = "version";
const char ver_help[] PROGMEM = "Print FW version";
const char ascii_cmd[] PROGMEM = "ascii";
const char ascii_help[] PROGMEM = "Print ASCII tables";
const char number_cmd[] PROGMEM = "number";
const char number_help[] PROGMEM =
    "Print and display matching number. Usage: number <decimal number>";

const char read_cmd[] PROGMEM = "read";
const char read_help[] PROGMEM = "Read UID of RFID tags";
const char print_cmd[] PROGMEM = "print";
const char print_help[] PROGMEM = "Print the list of RFID tags which were saved";
const char add_cmd[] PROGMEM = "add";
const char add_help[] PROGMEM =
    "Please add UID of card(s ) into the memory of device.";
const char delete_cmd[] PROGMEM = "delete";
const char delete_help[] PROGMEM =
    "Delete UID of card(s) from the memory of device.";
const char mem_cmd[] PROGMEM = "mem";
const char mem_help[] PROGMEM =
    "Print memory usage and change compared to previous call";


const cli_cmd_t cli_cmds[] =
{
    {help_cmd, help_help, cli_print_help, 0},
    {ver_cmd, ver_help, cli_print_ver, 0},
    {example_cmd, example_help, cli_example, 3},
    {ascii_cmd, ascii_help, cli_print_ascii_tbls, 0},
    {number_cmd, number_help, cli_handle_number, 1},
    {read_cmd, read_help, cli_rfid_read, 0},
    {print_cmd, print_help, cli_rfid_print, 0},
    {add_cmd, add_help, cli_rfid_add, 2},
    {delete_cmd, delete_help, cli_rfid_delete, 1},
    {mem_cmd, mem_help, cli_mem, 0}
};


/* Print list of available commands */
void cli_print_help(const char *const *argv)
{
    (void) argv;
    uart0_puts_p(PSTR("Implemented commands:\r\n"));

    for (uint8_t i = 0; i < NUM_ELEMS(cli_cmds); i++)
    {
        uart0_puts_p(cli_cmds[i].cmd);
        uart0_puts_p(PSTR(" : "));
        uart0_puts_p(cli_cmds[i].help);
        uart0_puts_p(crlf);
    }
}


/* 'example' takes three strings and prints them */
void cli_example(const char *const *argv)
{
    uart0_puts_p(PSTR("Command had following arguments:\r\n"));

    for (uint8_t i = 1; i < 4; i++)
    {
        uart0_puts(argv[i]);
        uart0_puts_p(crlf);
    }
}


/* Print fideleteware version */
void cli_print_ver(const char *const *argv)
{
    (void) argv;
    uart0_puts_p(ver_fw);
    uart0_puts_p(ver_libc);
}


/* Print ASCII chart */
void cli_print_ascii_tbls(const char *const *argv)
{
    (void) argv;
    /* Create and fill an array of ASCII characters */
    unsigned char ascii_array[ASCII_CHARS];
    int i = 0;

    while (i < ASCII_CHARS)
    {
        ascii_array[i] = i;
        i++;
    }

    /* Print ASCII charts into stdout */
    print_ascii_tbl(); /* Printable ASCII characters only */
    print_for_human(ascii_array, i); /* Here i = ASCII_CHARS */
}


/* Take a number and print a corresponding string */
void cli_handle_number(const char *const *argv)
{
    uint8_t num;

    /* Check whether the input is a number. sscanf is bloated... */
    for (size_t i = 0; i < strlen(argv[1]); i++)
    {
        if (!isdigit(argv[1][i]))
        {
            uart0_puts_p(PSTR("Please enter a decimal number.\r\n"));
            return;
        }
    }

    num = atoi(argv[1]); /* All OK, convert to integer */
    /* Clear the second row of LCD display, move to its start */
    lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
    lcd_goto(LCD_ROW_2_START);

    if (num <= 9) /* uint is always non-negative */
    {
        uart0_puts_p(PSTR("You entered number "));
        uart0_puts_p(pgm_read_ptr(&number_literal[num]));
        uart0_puts_p(PSTR(".\r\n"));
        lcd_puts_P(pgm_read_ptr(&number_literal[num]));
    }
    else
    {
        uart0_puts_p(PSTR("Please enter a number between 0 and 9!"));
        uart0_puts_p(crlf);
        lcd_puts_P(PSTR("Not valid number"));
    }
}


/* Error message for unknown commands */
void cli_print_cmd_error(void)
{
    uart0_puts_p(PSTR("Command not implemented.\r\n\tUse <help> to get help.\r\n"));
}


/* Error message for wrong number of arguments */
void cli_print_cmd_arg_error(void)
{
    uart0_puts_p(
        PSTR("Too few or too many arguments for this command\r\n\tUse <help>\r\n"));
}



/* Näitame mälukasutamise statiistikat.
 * Allikas: https://gitlab.com/I237-2017/code-snippets/snippets/1688520*/
void cli_mem(const char *const *argv)
{
    (void) argv;
    char print_buf[16] = {0x00};
    extern int __heap_start, *__brkval;
    int v;
    int space;
    static int prev_space;
    space = (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
    uart0_puts_p(PSTR("Heap statistics\r\n"));
    uart0_puts_p(PSTR("Used: "));
    ltoa(getMemoryUsed(), print_buf, 10);
    uart0_puts(print_buf);
    uart0_puts_p(PSTR("\r\nFree: "));
    ltoa(getFreeMemory(), print_buf, 10);
    uart0_puts(print_buf);
    uart0_puts_p(PSTR("\r\n\r\nSpace between stack and heap:\r\nCurrent  "));
    ltoa(space, print_buf, 10);
    uart0_puts(print_buf);
    uart0_puts_p(PSTR("\r\nPrevious "));
    ltoa(prev_space, print_buf, 10);
    uart0_puts(print_buf);
    uart0_puts_p(PSTR("\r\nChange   "));
    ltoa(space - prev_space, print_buf, 10);
    uart0_puts(print_buf);
    uart0_puts_p(PSTR("\r\n\r\nFreelist\r\nFreelist size:             "));
    ltoa(getFreeListSize(), print_buf, 10);
    uart0_puts(print_buf);
    uart0_puts_p(PSTR("\r\nBlocks in freelist:        "));
    ltoa(getNumberOfBlocksInFreeList(), print_buf, 10);
    uart0_puts(print_buf);
    uart0_puts_p(PSTR("\r\nLargest block in freelist: "));
    ltoa(getLargestBlockInFreeList(), print_buf, 10);
    uart0_puts(print_buf);
    uart0_puts_p(PSTR("\r\nLargest freelist block:    "));
    ltoa(getLargestAvailableMemoryBlock(), print_buf, 10);
    uart0_puts(print_buf);
    uart0_puts_p(PSTR("\r\nLargest allocable block:   "));
    ltoa(getLargestNonFreeListBlock(), print_buf, 10);
    uart0_puts(print_buf);
    uart0_puts_p(crlf);
    prev_space = space;
}


int cli_execute(int argc, const char *const *argv)
{
    // Move cursor to new line. Then user can see what was entered.
    //FIXME Why microrl does not do it?
    uart0_puts_p(PSTR("\r\n"));

    for (uint8_t i = 0; i < NUM_ELEMS(cli_cmds); i++) {
        if (!strcmp_P(argv[0], cli_cmds[i].cmd)) {
            // Test do we have correct arguments to run command
            // Function arguments count shall be defined in struct
            if ((argc - 1) != cli_cmds[i].func_argc) {
                cli_print_cmd_arg_error();
                return 0;
            }

            // Hand argv over to function via function pointer,
            // cross fingers and hope that funcion handles it properly
            cli_cmds[i].func_p (argv);
            return 0;
        }
    }

    cli_print_cmd_error();
    return 0;
}
