#ifndef HMI_MSG_H
#define HMI_MSG_H

extern const char ver_fw[];
extern const char ver_libc[];
extern const char stud_name[];
extern const char number_ask[];
extern const char crlf[];
extern const char out_of_memory[];
extern const char accessDenied[];
extern PGM_P const number_literal[];

#endif /* HMI_MSG_H */
