#ifndef RFID_H
#define RFID_H

#include <time.h>

#define DISPLAY_UPDATE_TIME 5
#define UID_MAX_LEN 10
#define LED_DOOR PORTA2 // pin 24
#define DOOR_OPEN_TIME 2

typedef struct card
{
    unsigned char uidSize;
    unsigned char uid[UID_MAX_LEN];
    char *name;
    struct card *jargmineKaart;
} kaart_tyyp;

typedef	enum
{
    uks_suletud,
    uks_sulgub,
    uks_avamine,
    uks_avaneb
} uks_olek_t;


typedef enum
{
    display_no_update,
    kuva_nimi,
    display_access_denied,
    display_clear
} ekraani_olek_t;

kaart_tyyp *firstCard;
time_t rfid_queries;
extern void rfid_scan(void);
void cli_rfid_read(const char *const *argv);
void cli_rfid_print(const char *const *argv);
void cli_rfid_add(const char *const *argv);
void cli_rfid_delete(const char *const *argv);
unsigned char uid_to_bytes(kaart_tyyp *card, const char *arg);
unsigned char hexchar_to_value(unsigned char c);

#endif /* RFID_H */
