#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include "hmi_msg.h"
#include "print_helper.h"
#include "rfid.h"
#include "andygock_avr-uart/uart.h"
#include "hd44780_111/hd44780.h"
#include "matejx_avr_lib/mfrc522.h"

kaart_tyyp *firstCard = NULL;
time_t rfid_queries = 0;

//RFID-tagide skaneerimine ja sõnum sellest, et kaart on leitud. Uks on avatud 2 sekundit, ja sõnum kestab 5 sekundit.

inline void rfid_scan(void)
{
    static time_t viimaneAeg, time_lastMsg, now, rfid_queries_prev;
    static uks_olek_t uks_olek;
    static ekraani_olek_t ekraani_olek;
    kaart_tyyp *jooksevKaart;
    static Uid uid, uid_eelmine; 
    char lcd_buffer[LCD_VISIBLE_COLS + 1] = {0x00};
    byte bufferATQA[2];
    byte bufferSize = sizeof(bufferATQA);
    now = time(NULL);

    if (rfid_queries_prev != rfid_queries)
    {
        byte result = PICC_WakeupA(bufferATQA, &bufferSize);

        if (result == STATUS_OK || result == STATUS_COLLISION)
        {
            jooksevKaart = firstCard;
            PICC_ReadCardSerial(&uid); 

            
// Kui kaart on vastu võetud, siis uuendab üle viimase kasutamise kuupäeva ja salvestab viimase kasutaja UID-d.

            if (memcmp(uid.uidByte, uid_eelmine.uidByte, UID_MAX_LEN) != 0)
            {
                uid_eelmine = uid;
                viimaneAeg = now;
            }

            while (jooksevKaart != NULL)
            {
                if (memcmp(uid.uidByte, jooksevKaart->uid, jooksevKaart->uidSize) == 0)
                {
                    uks_olek = uks_avamine;
                    ekraani_olek = kuva_nimi;
                    break; 
                }
                else
                {
                    jooksevKaart = jooksevKaart->jargmineKaart;
                }
            }

// Juhul kui kaard oli loetud, aga seda pole leidnud salvestatud kaartide sees, siis peab süsteem panema ukse kinni ja unustama kaardi (mis tähendab "access denied").
    if (jooksevKaart == NULL)
    {
      uks_olek = uks_sulgub;
      ekraani_olek = display_access_denied;
    }
	PICC_HaltA(); 
        }

//Kontollib aega, mis kuulutakse ukse avamiseks.
 switch (uks_olek)
        {
       	    case uks_avamine:
            PORTA |= _BV(LED_DOOR);
            uks_olek = uks_avamine;
            break;

            case uks_avaneb:
            if ((now - viimaneAeg) >= DOOR_OPEN_TIME)
            {
                memcpy(uid_eelmine.uidByte, "\0", UID_MAX_LEN);
                uks_olek = uks_sulgub;
            }

            break;

        case uks_sulgub:
        PORTA &= ~_BV(LED_DOOR);
        uks_olek = uks_suletud;
        break;

        case uks_suletud:
            break;

        default:
            break;
        }

switch (ekraani_olek)
        {
        case kuva_nimi:
            if (jooksevKaart->name != NULL)
            {
                strncpy(lcd_buffer, jooksevKaart->name, LCD_VISIBLE_COLS);
            }
            else
            {
                strcpy_P(lcd_buffer, PSTR("Cannot read name"));
            }

            time_lastMsg = now;
            ekraani_olek = display_clear;
            break;

        case display_access_denied:
            strcpy_P(lcd_buffer, accessDenied);
            time_lastMsg = now;
            ekraani_olek = display_clear;
            break;

        case display_clear:
            if ((now - time_lastMsg) >= DISPLAY_UPDATE_TIME)
            {
                strcpy(lcd_buffer, "\0");
                ekraani_olek = display_no_update;
            }

            break;

        case display_no_update:
            break;

        default:
            break;
        }

        rfid_queries_prev = rfid_queries;
    }
}

//Siin loetakse RFID kaardi ja näidatakse andmeid
//Siin ka hoiakse uID

void cli_rfid_read(const char *const *argv)
{
(void) argv;
    Uid uid;
    byte bufferATQA[2];
    byte bufferSize = sizeof(bufferATQA);
    byte result = PICC_WakeupA(bufferATQA, &bufferSize);

    if (result == STATUS_OK || result == STATUS_COLLISION)
    {
        PICC_ReadCardSerial(&uid);
        uart0_puts_p(PSTR("Card selected!\r\nUID size: 0x"));
        print_char_hex(uid.size);
        uart0_puts_p(PSTR("\r\nType: "));
        uart0_puts(PICC_GetTypeName(PICC_GetType(uid.sak)));
        uart0_puts_p(PSTR("\r\nUID: "));

        for (byte i = 0; i < uid.size; i++)
        {
            print_char_hex(uid.uidByte[i]);
        }

        uart0_puts_p(crlf);
        PICC_HaltA();
    }
    else
    {
        uart0_puts_p(PSTR("Unable to select card.\r\n"));
    }
}


// Prindib välja andmeid, mis teame kaardist.
void cli_rfid_print(const char *const *argv)
{
    (void) argv;
    unsigned int i, cards;
    char buffer[4];
    kaart_tyyp *jooksevKaart;
    jooksevKaart = firstCard;
    cards = 0;

    if (jooksevKaart == NULL)
    {
        uart0_puts_p(PSTR("No cards on the list.\r\n"));
    }
    else
    {
// Kontrollib, kui see kaard on üldse olemas.
        while (jooksevKaart != NULL)
        {
       itoa(++cards, buffer, 10);
       uart0_puts(buffer);
       uart0_puts_p(PSTR(". Card UID ("));
       itoa(jooksevKaart->uidSize, buffer, 10);
       uart0_puts(buffer);
       uart0_puts_p(PSTR(" bytes): "));

// Kirjutab uID-t.
            for (i = 0; i < jooksevKaart->uidSize; i++)
            {
                print_char_hex(jooksevKaart->uid[i]);
            }

            uart0_puts_p(PSTR(", owner name: "));
            uart0_puts(jooksevKaart->name); /* Name */
            uart0_puts_p(crlf); /* Go to new line */
            jooksevKaart = jooksevKaart->jargmineKaart; /* Go on to next card */
        }
    }
}


// Lisame kaardi meie baasi listile.
void cli_rfid_add(const char *const *argv)
{
    kaart_tyyp *newCard, *jooksevKaart, *lastCard;
    jooksevKaart = firstCard;
    lastCard = firstCard;
    char buffer[4];
    newCard = malloc(sizeof(kaart_tyyp));

    if (newCard == NULL)
    {
        uart0_puts_p(out_of_memory);
        return;
    }

    if (uid_to_bytes(newCard, argv[1])) 
    {

        while (jooksevKaart != NULL)
        {
            if (memcmp(newCard->uid, jooksevKaart->uid, jooksevKaart->uidSize) == 0)
            {
                uart0_puts_p(PSTR("Card already on the list.\r\n"));
                free(newCard);
                return;
            }
            else
            {
                lastCard = jooksevKaart;
                jooksevKaart = jooksevKaart->jargmineKaart;
            }
        }

        newCard->name = malloc(strlen(argv[2]) + 1); 

        if (newCard->name == NULL) 
        {
            uart0_puts_p(out_of_memory);
            free(newCard);
            return;
        }

        strcpy(newCard->name, argv[2]);
        uart0_puts_p(PSTR("Card added with UID: "));

        for (int posUid = 0; posUid < newCard->uidSize; posUid++)
        {
            print_char_hex(newCard->uid[posUid]);
        }

        itoa(newCard->uidSize, buffer, 10);
        uart0_puts_p(PSTR(" ("));
        uart0_puts(buffer);
        uart0_puts_p(PSTR(" bytes), owner name: "));
        uart0_puts(newCard->name);
        uart0_puts_p(crlf);
        newCard->jargmineKaart = NULL;

        if (firstCard == NULL)
        {
            firstCard = newCard;
        }
        else
        {
            lastCard->jargmineKaart = newCard;
        }
    }
    else
    {
        uart0_puts_p(PSTR("Could not add a card.\r\n"));
        free(newCard);
    }
}


// Kaardi kustatamine listist.
void cli_rfid_delete(const char *const *argv)
{
    kaart_tyyp tempCard;
    kaart_tyyp *jooksevKaart, *lastCard;
    jooksevKaart = firstCard;
    lastCard = firstCard;

    if (firstCard == NULL) 
    {
        uart0_puts_p(PSTR("No cards on the list, can't remove anything.\r\n"));
        return;
    }

// Kaardi nimekiri peab säilima andmemälus kuni mikrokontrolleri taaskäivitamiseni

    if (uid_to_bytes(&tempCard, argv[1]))
    {
        while (jooksevKaart != NULL)
        {
            if (memcmp(tempCard.uid, jooksevKaart->uid, tempCard.uidSize) == 0)
            {
                uart0_puts_p(PSTR("Removing card UID: "));

                for (int posUid = 0; posUid < jooksevKaart->uidSize; posUid++)
                {
                    print_char_hex(jooksevKaart->uid[posUid]);
                }

                uart0_puts_p(PSTR(", owner name: "));
                uart0_puts(jooksevKaart->name);
                uart0_puts_p(crlf);
                free(jooksevKaart->name);
                if (jooksevKaart == firstCard)
                {
                    firstCard = jooksevKaart->jargmineKaart;
                }
                else
                {
                    lastCard->jargmineKaart = jooksevKaart->jargmineKaart;
                }

                free(jooksevKaart);
                return;
            }
            else
            {
                lastCard = jooksevKaart;
                jooksevKaart = jooksevKaart->jargmineKaart;
            }
        }
        uart0_puts_p(PSTR("No matches found.\r\n"));
    }
    else
    {
        uart0_puts_p(PSTR("Could not remove the card.\r\n"));
    }
}

unsigned char uid_to_bytes(kaart_tyyp *card, const char *arg)
{
    size_t posArg, posUid;
    size_t len = strlen(arg);

    /* UID longer than 10 bytes not accepted */
    if (len > 2 * UID_MAX_LEN)
    {
        uart0_puts_p(PSTR("UID too long.\r\n"));
        return 0;
    }

    /* The input should be hexadecimal */
    for (posArg = 0; posArg < len; posArg++)
    {
        if (!isxdigit(arg[posArg]))
        {
            uart0_puts_p(PSTR("Invalid UID provided (not hex).\r\n"));
            return 0;
        }
    }

    posArg = 0;
    posUid = 0; 
    card->uidSize = len / 2;
    if (len & 0x01)
    {
        card->uidSize++;
        card->uid[posUid++] = hexchar_to_value(arg[posArg++]);
    }

    for (; posArg < len; posUid++, posArg++)
    {
        card->uid[posUid] = hexchar_to_value(arg[posArg++]) << 4;
        card->uid[posUid] |= hexchar_to_value(arg[posArg]);
    }

    return 1;
}

// Hex sümboli konverteerimine oma väätusele. Kui on antud mitte Hex sümbol, siis vastab nulliga.

unsigned char hexchar_to_value(unsigned char c)
{
    if (c >= '0' && c <= '9')
    {
        c -= 0x30;
    }
    else if (c >= 'A' && c <= 'F')
    {
        c -= 0x37;
    }
    else if (c >= 'a' && c <= 'f')
    {
        c -= 0x57;
    }
    else
    {
        c = 0;
    }

    return c;
}
